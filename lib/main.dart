// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return MaterialApp(
      title: 'Mississippi',
      home: Mississippi(),
    );
  }
}

class Mississippi extends StatefulWidget {
  @override
  MississippiState createState() => MississippiState();
}

class MississippiState extends State<Mississippi> {
  final _suggestions = <String>[];
  final _biggerFont = const TextStyle(fontSize: 18.0);
  final _hugeFont = const TextStyle(fontSize: 24.0);

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mississippi Generator'),
      ),
      body: _buildSuggestions(),
    );
  }

  Widget _buildSuggestions() {
    return ListView.builder(
      padding: const EdgeInsets.all(16.0),
      itemBuilder: /*1*/ (ctx, i) {
        if (i.isOdd) return Divider(); /*2*/

        final index = i ~/ 2; /*3*/
        if (index >= _suggestions.length) {
          _suggestions.addAll(generateMississippis(10, index)); /*4*/
        }
        return _buildRow(_suggestions[index], ctx);
      }
    );
  }

  Iterable<String> generateMississippis([int num=10, int start=0]) sync* {
    int i = start;
    while (i < start+num) {
      int div = i ~/ 16;
      int r = Random().nextInt(1+ 2*div);
      String s = (i-div + r).toString();

      s += ' Mississippi';
      yield s;
      i++;
    }
  }

  Widget _buildRow(String mississippi, BuildContext ctx) {
    return ListTile(
      title: Text(
        mississippi,
        style: _biggerFont,
      ),
      onTap: () {
        _wait(ctx);
      },
    );
  }

  void _wait(ctx) {
    Navigator.of(ctx).push(
      MaterialPageRoute<void>(
        builder: (BuildContext ctx) {
          return Scaffold(         // Add 6 lines from here...
            appBar: AppBar(
              title: Text('Waiting...'),
            ),
            body: Text(
              'X Mississippi',
              style: _hugeFont,
            ),
          );
        },
      ),
    );
  }
}
